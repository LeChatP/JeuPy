#include "fichier.h"

UserList *USERS;

jmp_buf openfile;
jmp_buf readfile;

void afficherUtilisateurs(){
	if(!setjmp(openfile))
	{
		FILE* f = openFile("rb");
		if(!setjmp(readfile))
		{
			readUsers(f);
		}
		else{
			printf("Il n'y a pas de classement pour le moment \n");
		}
		fclose(f);
	}else
	{
		printf("Le fichier est inexistant ou inaccesible\n");
	}
	return;
}

void readUsers(FILE *f)
{
	UserData first = readUser(f);
	if(first.tentatives == 0) longjmp(readfile,1);
	UserList *ul = creer_userlist(first);
	while(!feof(f)){
		UserData ud = readUser(f);
		if(ud.tentatives == 0) break;
		append_user(ul,ud);
	}
	sort(&ul);
	printList(ul);
}
//lit un utilisateur et renvoie sous forme de structure
UserData readUser(FILE *f)
{
	UserData user;
	user.tentatives = 0;
	user.temps = 0;
	size_t taille = 0;
	if(fscanf(f,"%d %d %u ", &user.tentatives, &user.temps, &taille)==EOF)return user;
	user.nom = malloc((taille+1)*sizeof(char));
	fscanf(f,"%s\n",user.nom);
	return user;
}

void enrUser(char* nom, int tentatives, int time){
	FILE* f = openFile("ab");
	fprintf(f,"%d %d %u %s\n", tentatives, time,strlen(nom), nom);
	fclose(f);
}

FILE* openFile(char* mode){
	FILE* f = fopen("users.txt",mode);
	if(f == NULL){
		longjmp(openfile,1);	
	}
	return f;
}

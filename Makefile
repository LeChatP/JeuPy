CC=gcc
PARAMS=-Wall -lwiringPi
FOLDER=build/

build: cd sed jeu

debug: cd sed_debug build_debug

jeu: jeu.c $(FOLDER)systeme.o $(FOLDER)clavier.o $(FOLDER)fichier.o $(FOLDER)code.o $(FOLDER)userlist.o
	$(CC) -o Jeu $^ $(PARAMS)

build_debug: jeu.c $(FOLDER)systeme.o $(FOLDER)clavier.o $(FOLDER)fichier.o $(FOLDER)code.o $(FOLDER)wiringPi.o $(FOLDER)userlist.o
	$(CC) -o Jeu $^ -Wall

$(FOLDER)systeme.o: systeme.c
	$(CC) -o $@ -c $< $(PARAMS)

$(FOLDER)clavier.o: clavier.c
	$(CC) -o $@ -c $< $(PARAMS)

$(FOLDER)fichier.o: fichier.c
	$(CC) -o $@ -c $< $(PARAMS)

$(FOLDER)code.o: code.c
	$(CC) -o $@ -c $< $(PARAMS)

$(FOLDER)wiringPi.o: wiringPi.c
	$(CC) -o $@ -c $< $(PARAMS)

$(FOLDER)userlist.o: userlist.c
	$(CC) -o $@ -c $< $(PARAMS)

sed:
	sed -i -e "s/az(/\/\/az(/g" jeu.c
	sed -i -e "s/#include \"wiringPi.h\"/#include <wiringPi.h>/g" systeme.h

sed_debug:
	sed -i -e "s/\/\/az(/az(/g" jeu.c
	sed -i -e "s/#include <wiringPi.h>/#include \"wiringPi.h\"/g" systeme.h

cd:
	mkdir -p $(FOLDER)
	cd $(FOLDER)

clean:
	mkdir -p $(FOLDER)
	rm -r $(FOLDER)
	rm -f Jeu

run:
	./Jeu

#ifndef CODE_H_
#define CODE_H_

#include <time.h>
#include <stdlib.h>

//crée le code
extern void creerCode();
// vide le code
extern void reinitialiserCode();
extern int verifierCode(char code);
int avancer();
extern char* char2bin(unsigned char c);

#endif
#ifndef SYSTEME_H_
#define SYSTEME_H_
#include "wiringPi.h"
#include <stdio.h>
#include <stdlib.h>


//fonction regroupant toutes les initialisation système
extern void initialiserSysteme();

//applique la fonction paramètre au bouton gauche
extern void setBPGfunction(void* function);
//applique la fonction paramètre au bouton droit
extern void setBPDfunction(void* function);

//GESTION LED
//eteint les LED rouge et verte
extern void resetStatusLED();
//applique l'etat en paramètre pour la led Rouge
extern void setRedLED(int position);
//applique l'etat en paramètre pour la led Verte
extern void setGreenLED(int position);

//GESTION Bargraph
//applique le meme état pour toutes les LED du bargraph
extern void setBargraph(int position);
//allume la led de la position en paramètre
extern void allumerPositionBargraph(int position);

extern void setPositionBargraph(int led, int position);

//effectue l'operation modulo de 8 pour tout i appartenant à Z
extern int mod8(int i);

//fonctions internes
void initialiserBargraph();
void wiringPi(int bp,void* function);

#endif

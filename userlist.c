#include "userlist.h"

//crée une nouvelle liste
UserList *creer_userlist (UserData user)
{
	UserList *list = malloc(sizeof(UserList));
	if (list)
 	{
		list->user = user;
		list->next = NULL;
	}
	return list;
}

//ajoute un élément a la liste
UserList *append_user(UserList *list, UserData user)
{
	UserList **plist = &list;
	while (*plist)
		plist = &(*plist)->next;
	*plist = creer_userlist(user);
	if (*plist)
		return list;
	else
		return NULL;
}

// renvoie le dernier element de la liste
UserList *dernierElement(UserList *elem)
{
	while (elem != NULL && elem->next != NULL)
		elem = elem->next;
	return elem;
}

//tri du tableau
void sort(UserList **debutRef)
{
	(*debutRef) = quickSort(*debutRef, dernierElement(*debutRef));
	return;
}
 
//tri par quicksort
UserList *quickSort(UserList *debut, UserList *fin)
{
	// debut n'est pas null ni égal à la fin
	if (!debut || debut == fin)
		return debut;
 
	UserList *newDebut = NULL, *newFin = NULL;
 
	// récupération du pivot + moitié gauche
	UserList *pivot = partition(debut, fin, &newDebut, &newFin);
 
 	//si le pivot est déjà le plus petit élement pas besoin d'itérer la première moitié
	if (newDebut != pivot)
	{
		// on coupe le fil temporairement de l'élément précédant le pivot
		UserList *tmp = newDebut;
		while (tmp->next != pivot)
			tmp = tmp->next;
		tmp->next = NULL;

 
		// on tri la première moitié : debut -> pivot -1
		newDebut = quickSort(newDebut, tmp);

 
		// on replace le fil
		tmp = dernierElement(newDebut);
		tmp->next =pivot;
	}
 
	// tri de la seconde moitié
	pivot->next = quickSort(pivot->next, newFin);

	return newDebut;
}

// partitionne la liste partant de la fin comme pivot
// retourne le pivot + les nouvelles positions de debut et fin
UserList *partition(UserList *debut, UserList *fin, UserList **newDebut, UserList **newFin)
{
	UserList *pivot = fin;
	UserList *prev = NULL, *cur = debut, *bout = pivot;
	//tant que le pivot et cur ne se rencontrent pas
	while (cur != pivot)
	{
		// on vérifie que cur < pivot à l'aide des variables
		if (cur->user.tentatives+(0.5*cur->user.temps) < pivot->user.tentatives +(0.5*pivot->user.temps))
		{
			// on échange la valeur plus faible en tant que debut
			if (*newDebut == NULL)
				*newDebut = cur;
			prev = cur;
			cur = cur->next;
		}
		// si cur est plus grand
		else
		{
			// on déplace cur après le bout et on réattribue le bout en tant que cur
			if (prev)
				prev->next = cur->next;
			UserList *tmp = cur->next;
			cur->next = NULL;
			bout->next = cur;
			bout = cur;
			cur = tmp;
		}
	}
	//si le pivot etait déjà le plus petit element, on l'attribue a newDebut
	if (*newDebut == NULL)
		*newDebut = pivot;
 
	// et on attribue le bout en tant que fin de la partition
	(*newFin) = bout;
 
	// retourne le pivot partiellement/complètement trié
	return pivot;
}

void printList(UserList *users)
{
	UserList *tmp = users;
	int i = 1;
	printf("%d : %s avec %i tentatives en %d secondes\n", i , tmp->user.nom, tmp->user.tentatives, tmp->user.temps);
	while (tmp->next != NULL)
	{
		i++;
		tmp = tmp->next;
		printf("%d : %s avec %i tentatives en %d secondes\n", i , tmp->user.nom, tmp->user.tentatives, tmp->user.temps);
	}
}
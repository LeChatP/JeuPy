#include "wiringPi.h"
char BARGRAPH[] = "0000000";
int pin22 = 0;
int pin26 = 0;

void digitalWrite(int pin, int position)
{
	if(pin < 8)
		BARGRAPH[pin] = position + '0';
	else if (pin == 22)
		pin22 = position;
	else if (pin == 26)
		pin26 = position;
	afficherPi();

}

void afficherPi(){
	printf("[ ");
	for(int i = 0 ; i < 8 ; i++)
	{
		if(BARGRAPH[(((i-1)%8)+8)%8] == '1'){
			printf("\033[33m▮ \033[0m");
		}else
		{
			printf("▯ ");
		}
	}
	printf("\033[0m]");
	if (pin22 == 1)
		printf("\033[31m [▮] \033[0m");
	else
		printf(" [▯] ");
	if(pin26 == 1)
		printf("\033[32m [▮] \033[0m\n");
	else
		printf(" [▯] \n");
}

void delay(int i){
	clock_t start,end;
    start=clock();
    while(((end=clock())-start)<=((i*CLOCKS_PER_SEC)/1000));
}
int digitalRead(int pin){
	return 0;
}

void az(void (*a)(),void (*b)())
{
	char c = getchar();
	if(c == 'a')
		(*a)();
	else if (c == 'z')
		(*b)();
}

void wiringPiSetup()
{
	return;
}
void pinMode(int pin,int mode)
{
	return;
}
int wiringPiISR(int i, int pos, void *function)
{
	return 0;
}

# Projet Raspberry Pi
## Présenté par Eddie BILLOIR et aidé par Ulysse WINUM
### IUT Paul Sabatier Rangueil DUT Informatique

Ce projet est un travail réalisé pour l'examen final du Module M2101
C'est un petit jeu qui demande a trouver le code en un minimum d'essais et de temps
Ce projet utilise : 
 * Makefile personnalisé gérant la compilation par segmentation, avec et sans Raspberry Pi
 * De la réallocation dynamique
 * Une structure de données
 * Une structure Liste (LinkedList)
 * Du Tri par QuickSort
 * Un Système de sauvegarde fichier
 * Gestion d'exception de lecture de fichier avec setjmp/longjump
 * Des Tableaux/chaines de caractères
 * Génération de nombre aléatoire
 * *Interruption de programme*

## Pré-requis

 * Linux
 * gcc
 * make
 * Raspberry Pi (optionnel)
   * 1 LED ROUGE Pin 22 
   * 1 LED VERTE Pin 26
   * 1 Bargraph  Pin 7,0,1,2,3,4,5,6
   * module wiringPi installé

## Compilation

Pour compiler avec un Raspberry Pi : 
 
   ./make build

Pour compiler sans Raspberry Pi : 

   ./make debug

## Utilisation

Avec le Raspberry Pi, Lors de la saisie clavier virtuel, il faut utiliser le bouton gauche pour se déplacer sur le clavier et sur le bouton droit pour valider la lettre.
Sans le raspberry, les boutons gauche droite sont remplacés par la touche 'a' et la touche 'z', pour valider la touche faire entrer. 
Puis utiliser les boutons gauche droite pour trouver le code secret de 8 touches

## Quelques Statistiques

 * 64 fonctions
 * 2 structures
 * 14 fichiers code (7 source 7 en-tête)
 * 3 malloc, 1 calloc, 1 realloc
 * 3 pointeurs de fonction
 * 33 printf


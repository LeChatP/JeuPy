#ifndef JEU_H_
#define JEU_H_

#include <time.h>

#include "clavier.h"
#include "fichier.h"
#include "systeme.h"
#include "code.h"

int main(void);

void icompteur();
void valider();

void BPG();
void BPD();
void checkBPGCode();
void checkBPDCode();
void checkBPCode(char c);

void debut();
void debutclignoter();
void gagneclignoter();
void gagne();
void perdu();

void reinitialiserJeu();

#endif


#include "clavier.h"
#define TAILLE_ALPHABET 27

char alphabet[TAILLE_ALPHABET] = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'X', 'Y', 'Z', '0', '1'};

char *PRENOM;
int TAILLE_PRENOM = 0;
int COMPTEUR = 0;

void initialiserPrenom(){
	COMPTEUR = 0;
	PRENOM = (char*) malloc(sizeof(char));
	PRENOM[0] = '\0';
	afficherAlphabet(COMPTEUR);
}

void reinitialiserPrenom(){
	COMPTEUR = 0;
	memset(PRENOM,0,TAILLE_PRENOM);
	TAILLE_PRENOM = 0;
}

char* getPrenom(){
	return PRENOM;
}

void lettreSuivante(){
	COMPTEUR = (COMPTEUR+1) %TAILLE_ALPHABET;
	afficherAlphabet(COMPTEUR);
}

char getLettreClavier(){
	return alphabet[COMPTEUR];
}
char getLettreAlphabet(int position){
	return alphabet[position];
}

int prenomEstVide(){
	return TAILLE_PRENOM==0;
}

void retirerLettre()
{
	if(prenomEstVide()){
		return;
	}
	TAILLE_PRENOM-=1;
	char *tmp = PRENOM;
	PRENOM = strncpy(PRENOM,tmp,TAILLE_PRENOM);
	PRENOM[TAILLE_PRENOM] = '\0';
	afficherAlphabet(COMPTEUR);
}

void ajouterLettre()
{
	incrementTailleNom();
	PRENOM[TAILLE_PRENOM-1] = alphabet[COMPTEUR];
	PRENOM[TAILLE_PRENOM] = '\0';
	afficherAlphabet(COMPTEUR);
	printf("Lettre %c ajoutée\n",PRENOM[TAILLE_PRENOM-1]);
}

void finSaisie()
{
	incrementTailleNom();
	PRENOM[TAILLE_PRENOM-1] = '\0';
	printf("Bienvenue %s\n",PRENOM);
}

void incrementTailleNom(){
	TAILLE_PRENOM++;
	char *buffer;
	buffer = (char*) realloc(PRENOM,TAILLE_PRENOM+1*sizeof(char));
	PRENOM = buffer;
}

void afficherAlphabet(int position){
	system("clear");
	if(TAILLE_PRENOM > 0)
	printf("%s\n", PRENOM);
    for (int i = 0 ; i < TAILLE_ALPHABET ; i++)
    {
	if (position == 26 && i == 26)
		printf("<OK>");
	else if (position == 25 && i == 25)
		printf("<SUPPR> ");
	else if (i == 26)
		printf("OK");
	else if (i == 25)
		printf("SUPPR ");
	else if (position == i)
 		printf("<%c> ", alphabet[i]);
	else
        printf("%c ", alphabet[i]);
    }
	printf("\n");
}

#include "jeu.h"

int POSITION = 7;
int ESSAIS = 1;
int FIN = 0;
time_t START_TIME, END_TIME;
int ELAPSED;
int BP = 0;

int main(void)
{
	initialiserSysteme();
	setBPGfunction(&BPG);
	setBPDfunction(&BPD);
	initialiserPrenom();
	debut();
	while (FIN==0){
		az(&BPG,&BPD);
	}
}


//ACTIONS BP
void BPG(){
	if(BP == 0) 
		icompteur();
	else if(BP > 0)
		checkBPGCode();
}

void BPD(){
	if(BP == 0) 
		valider();
	else if(BP > 0)
		checkBPDCode();
}

//

void icompteur(){
	lettreSuivante();
}

void valider(){
	if(getLettreClavier()=='0') {
		retirerLettre();
	}
	else if (getLettreClavier()=='1') {
		if(prenomEstVide()){
			printf("Merci d'écrire un nom");
			return;
		}
		finSaisie();
		creerCode();
		BP=1;
		printf("Le code a été créé\ntrouvez-le a l'aide des boutons gauche et droite\nLe Chronomètre est lancé\nBonne chance :)\n");
		time(&START_TIME);
		return;
	}
	else{
		ajouterLettre();
	}
}


void checkBPGCode(){
	checkBPCode('0');
	delay(100);
}

void checkBPDCode(){
	checkBPCode('1');
	delay(100);
}

void checkBPCode(char c){
	resetStatusLED();
	int i = verifierCode(c);
	if (i >= 0){
		POSITION = mod8(POSITION+1);
		allumerPositionBargraph(mod8(POSITION-1));
		if (i > 0)gagne();
	}
	else {
		POSITION = 7;
		perdu();
	}
}

// RESULTAT

void debut(){
	BP=0;
	resetStatusLED();
	printf("Bienvenue sur ce mini-jeu, vous devez ecrire votre nom en premier\nPuis trouver le code en un minimum de temps et en un minimum d'essais\n");
	printf("Voici le classement des gagnants : \n");
	afficherUtilisateurs();
	debutclignoter();
}

void debutclignoter(){
	for(int i = 0 ; i < 27; i++){
		char *lettre = char2bin(getLettreAlphabet((i*7)%27));
		for (int j = 0 ; j < 8 ; j++)
		{
			setPositionBargraph(mod8(j-1), lettre[j] - '0');
			delay(5);
		}
		delay(100);
	}
	setBargraph(LOW);
}

void gagneclignoter(){
	setBargraph(LOW);
	for(int i = 0 ; i < 4; i++){
		setGreenLED(HIGH);
		for (int j = 0 ; j < 8 ; j++)
		{
			allumerPositionBargraph(mod8(j-1));
			setPositionBargraph(mod8(j-2), LOW);
			delay(100);
		}
		delay(100);
		setBargraph(LOW);
		setGreenLED(LOW);
	}
	setBargraph(LOW);
	delay(100);
	setBargraph(HIGH);
	delay(100);
	setBargraph(LOW);
	setGreenLED(HIGH);
}

void gagne(){
	time(&END_TIME);
	ELAPSED = END_TIME - START_TIME;
	gagneclignoter();
	printf("Bravo tu as réussi!\n");
	printf("Nombre de tentatives : %d\n temps passé : %dsecondes\n", ESSAIS,ELAPSED);
	enrUser(getPrenom(), ESSAIS, ELAPSED);
	printf("Tu es enregistré dans la liste !\n");
	printf("Continuer ? Y/n\n");
	BP = -1;
	char value = getchar();
	if (value == 'n' || value == 'N') {
		FIN = -1;
		return;
	}else {
		reinitialiserJeu();
	}
	
}

void perdu(){
	setRedLED(HIGH);
	setBargraph(LOW);
	delay(100);
	setBargraph(HIGH);
	delay(100);
	setBargraph(LOW);
	printf("Dommage, essaie encore!\n");
	reinitialiserCode();
	ESSAIS++;
}

void reinitialiserJeu(){
	reinitialiserCode();
	ESSAIS = 1;
	reinitialiserPrenom();
	POSITION = 7;
	setBargraph(LOW);
	resetStatusLED();
	debut();
}

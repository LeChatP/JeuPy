#ifndef FICHIER_H_
#define FICHIER_H_

#include <setjmp.h>
#include <string.h>

#include "userlist.h"

//FICHIER data.c

//affiche la liste des gagnants 
//avec leur nom et leur nombre de tentatives
//trié et
extern void afficherUtilisateurs();

//ajoute dans le fichier des gagnants le gagnant
extern void enrUser(char* nom, int tentatives, int time);
FILE* openFile(char *mode);

//gestion lecture fichier
void readUsers(FILE *f);
void reallocUserData(UserList *array, UserData user);
UserData readUser(FILE *f);

#endif
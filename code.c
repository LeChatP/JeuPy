#include "code.h"

char *CODE;
int POSITION_CODE = 0;

void creerCode(){
	srand(clock());
	unsigned char c = (unsigned char) (rand()%(unsigned char)-1);
	CODE = char2bin(c);
}

int verifierCode(char code){
	if (CODE[POSITION_CODE] == code)
		return avancer();
	else
		return -1;
	
}

void reinitialiserCode(){
	POSITION_CODE = 0;
}

int avancer(){
	if(POSITION_CODE == 7) {
		return 1;
	}
	POSITION_CODE++;
	return 0;
}

char* char2bin(unsigned char c)
{
	char *code = calloc(9,sizeof(char));
	int i = 0;
	while (i < 8) {
    	if (c & 1)
        		code[i] = '1';
   		else
        		code[i] = '0';
    		c >>= 1;
		i++;
	}
	code[i] = '\0';
	return code;
}
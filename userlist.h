#ifndef USERLIST_H_
#define USERLIST_H_

#include <stdlib.h>
#include <stdio.h>

//Structure ayant le nom du joueur et son nombre de tentatives
typedef struct{
	int tentatives;
	int temps;
	char *nom;
} UserData;

typedef struct UserList UserList;
struct UserList{
	UserList *next;
	UserData user;
};

//ajoute un utilisateur a la liste
extern UserList *append_user(UserList *list, UserData user);
//créer une liste d'utilisateurs
extern UserList *creer_userlist (UserData data);
//renvoie le dernier élément de la liste en paramètre
extern UserList *dernierElement(UserList *elem);

extern void printList(UserList *users);

//tri
extern void sort(UserList **debutRef);
UserList *quickSort(UserList *debut, UserList *fin);
UserList *partition(UserList *debut, UserList *fin, UserList **newDebut, UserList **newFin);

#endif
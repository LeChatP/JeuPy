#include "systeme.h"

#define BPG 21
#define BPD 29
#define LDR 22
#define LDV 26

void initialiserSysteme(){
	wiringPiSetup();
	initialiserBargraph();
	pinMode(BPG,INPUT);
	pinMode(BPD,INPUT);
	pinMode(LDR,OUTPUT);
	pinMode(LDV,OUTPUT);
}

extern void setBPGfunction(void* function){
	wiringPi(BPG,function);
}
extern void setBPDfunction(void* function){
	wiringPi(BPD,function);
}

void initialiserBargraph(){
	for(int lb =0; lb < 8; lb++)
	{
		pinMode(lb,OUTPUT);
	}
}

void resetStatusLED(){
	setRedLED(LOW);
	setGreenLED(LOW);
}

void setRedLED(int position){
	digitalWrite(LDR,position);
}

void setGreenLED(int position){
	digitalWrite(LDV,position);
}

void setBargraph(int position){
	for (int i = 0; i < 8; i++)
	{
		digitalWrite(mod8(i-1), position);
	}
}

void allumerPositionBargraph(int position)
{
	digitalWrite(position, HIGH);
}

void setPositionBargraph(int led, int position)
{
	digitalWrite(led, position);
}

void wiringPi(int bp,void* function){
	if(wiringPiISR(bp,INT_EDGE_FALLING, function)){
		printf("Impossible de mettre en place l'association de %d",bp);
		exit(-1);
	}
}

int mod8(int i){
	return ((i%8)+8)%8;
}

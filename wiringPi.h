#ifndef WIRINGPI_H_
#define WIRINGPI_H_

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define INPUT 0
#define OUTPUT 1
#define LOW 0
#define HIGH 1
#define INT_EDGE_FALLING 0
#define TRUE 1
#define FALSE 0 

extern void wiringPiSetup();
extern void pinMode(int pin,int mode);
extern void digitalWrite(int pin, int position);
extern void delay(int i);
extern int wiringPiISR(int i, int pos, void *function);
extern int digitalRead(int pin);

extern void az(void (*a)(),void (*b)());

void afficherPi();

#endif

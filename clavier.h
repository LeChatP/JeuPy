#ifndef CLAVIER_H_
#define CLAVIER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern void initialiserPrenom();
extern void reinitialiserPrenom();
extern void lettreSuivante();
extern void ajouterLettre();
extern void retirerLettre();
extern void finSaisie();
extern char* getPrenom();
extern char getLettreClavier();
extern char getLettreAlphabet(int position);
extern int prenomEstVide();

void incrementTailleNom();
void afficherAlphabet();

#endif